|参数名|类型|说明|可选项|
---|---|---|--
type | string | 上报的数据类型 | pv/perf/pv/api
page | string | 发生请求的页面地址 | 
tag  | string | 用户配置的页面标记
api  | string | 请求的api地址
success | number | 请求是否成功 | 1:成功,0:失败
time | number | 请求耗时 | 
code | number | 请求返回的code码
msg  | string | 请求返回的data里message的值
sr   | string | 用户设备的分辨率
vp   | string | 用户设备的视口大小
uid  | string | 自动分配给用户的uid|
pid  | string | 项目初始化时填入的pid


> 操作系统(os),浏览器(browser)信息通过后端获取请求header里的UserAg  


每种数据上报都有的公共字段：

- uid
- pid
- vp
- sr
- tag
- type
- page