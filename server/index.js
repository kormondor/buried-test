const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');

let app = express()



app.use('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*'); //这个表示任意域名都可以访问，这样写不能携带cookie了。
    //res.header('Access-Control-Allow-Origin', 'http://www.baidu.com'); //这样写，只有www.baidu.com 可以访问。
    res.header('Access-Control-Allow-Headers', 'Content-Type, Content-Length, Authorization, Accept, X-Requested-With , yourHeaderFeild');
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS'); //设置方法
    if (req.method == 'OPTIONS') {
        res.send(200); // 意思是，在正常的请求之前，会发送一个验证，是否可以请求。
    } else {
        next();
    }
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(bodyParser.text());

app.post('/', (req, res) => {
    setTimeout(() => {
        res.status(200),
            res.send({
                code: 200,
                message: 'success',
                data: {
                    name: 'Jom'
                }
            })
    }, 500);
})

app.get('/img', (req, res) => {
    // console.log(req.query);
    let a = fs.readFileSync('1.jpg')
    res.send(a)
})

app.post('/img', (req, res) => {
    console.log(req.body);
    res.send("ok")
})



app.listen(3000, 'localhost', () => {
    console.log('listening in port 3000')
})