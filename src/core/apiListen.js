import {
    saveStorage
} from '../utils/lib/lib';
import {
    mergeLog
} from './dataFormat';
import {
    sendLog
} from './sendlog';


import ignore from './ignore';

export function setAjaxInfo() {
    let now;
    let ajaxInfo = {};
    const handler = (e) => {
        if (ignore.isArrayHasUrl.call(this, e.detail.responseURL) > 0) return;
        if (e.detail.readyState === 1) {
            now = new Date().getTime();
        }
        if (e.detail.readyState === 4) {
            ajaxInfo.type = 'api'
            ajaxInfo.time = new Date().getTime() - now;
            ajaxInfo.success = e.detail.status === 200 ? 1 : 0;
            // ajaxInfo.response = e.detail.responseText
            ajaxInfo.api = e.detail.responseURL
            // ajaxInfo.type = e.detail.statusText
            ajaxInfo.msg = e.detail.responseText ? JSON.parse(e.detail.responseText).message : ''
            ajaxInfo.code = e.detail.responseText ? JSON.parse(e.detail.responseText).code : ''
            // setAjaxStorage(ajaxInfo)
            if (this._conf.disableHook) {
                saveStorage('ajaxList', mergeLog.call(this, ajaxInfo))
            } else {
                let wholeData = mergeLog.call(this, ajaxInfo);
                this.sendLog(wholeData)
                // this.sendLog(dataToGet.call(this, ajaxInfo))
            }
        }
    }
    window.addEventListener('ajaxReadyStateChange', handler)
}




export default {
    setAjaxInfo
}