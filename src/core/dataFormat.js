// let testData = {
//     "time": 819,
//     "status": 200,
//     "response": "{\"code\":200,\"message\":\"success\",\"data\":{\"name\":\"Jom\"}}",
//     "url": "http://localhost:3000/",
//     "type": "OK"
// }

export function dataToGet(data) {
    let {
        entries
    } = Object;
    let res = [];
    for (let [key, value] of entries(data)) {
        res.push(`${key}=${value}`);
    }
    return '?' + res.join('&')
}

export function mergeLog(data) {
    return Object.assign(data, this.getInfo().usual);
}




export default {
    dataToGet
}