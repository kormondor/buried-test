function getIgnoreList() {
    return this._conf.ignoreUrls || []
}

//  ['alicnd','baidu']   http://www.baidu.com

/**
 *
 *判断当前的API地址是否在忽略列表里
 * @param {String} target
 * @returns {Number} 大于0代表请求地址存在于忽略列表里，否则不存在
 */
function isArrayHasUrl(target) {
    let arr = getIgnoreList.call(this);
    return arr.filter((v, i) => target.includes(v)).length;
}



export default {
    getIgnoreList,
    isArrayHasUrl
}