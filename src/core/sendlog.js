import {
    getStringLength
} from '../utils/lib/lib'

import {dataToGet} from '../core/dataFormat';

export function sendLog(data = null) {
    
    let urlData = dataToGet(data);
    let length = getStringLength(urlData);
    let imgUrl = this._conf.imgUrl;
    if (length < 2000) {
        sendImgLog(imgUrl + urlData)
    } else if (length > 2000 && window.navigator.sendBeacon) {
        sendBeaconLog(imgUrl,data)
    } else {
        sendXHRLog(imgUrl,data)
    }
}


export function sendImgLog(url) {
    let img = new Image();
    img.src = url;
}


export function sendBeaconLog(url, data) {
    let a = navigator.sendBeacon(url, JSON.stringify(data));
}

let data = {
    a: 1,
    b: 2
};

export function sendXHRLog(url, data) {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', url, false);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(data))
}