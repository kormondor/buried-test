import apiListen from './apiListen';

const core = {};

export default Object.assign(core, apiListen)