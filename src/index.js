import utils from './utils/lib/index';
import './utils/lib/xmlhttprequest';
import core from './core/index';
import axios from 'axios';
import {
    sendLog
} from './core/sendlog';

export default (function (global, document, undefined) {

    let config = {
        ver: '0.1.1',
        appType: "web",
        ignoreUrls: [],
        imgUrl: 'http://localhost:3000',
        disableHook: false,
    }



    function Seye(config) {
        this._conf = config
        this.ver = '.0.1.1'
        this.setAjaxInfo = core.setAjaxInfo
        this.setAjaxInfo()
    }

    /**
     * 监听器只使用单例模式
     * ProxySingletonSeye是生成Seye单例的方法
     */
    const ProxySingletonSeye = (
        function () {
            let instance;
            return function (config) {
                if (!instance) {
                    instance = new Seye(config)
                }
                return instance
            }
        }
    )()
    /**
     * 获取用户侧及performance信息
     */
    Seye.prototype.getInfo = function () {
        let device = utils.getDevice();
        let ua = utils.getUA();
        let os = utils.getOS();
        let pr = utils.getDpi();
        let networkPerformance = utils.getNetworkPerformance();
        let page = utils.getUrl().href;
        let position = utils.canUseGeolocation() ? utils.getPosition() : null
        let vp = utils.getVp();
        return {
            usual: {
                vp,
                pr,
                page,
                pid: this._conf.pid,
                tag: this._conf.tag || '',
                begin: new Date().getTime()
            },
            device,
            ua,
            os,
            networkPerformance,
            position
        }
    }

    /**
     * 发送日志
     */
    Seye.prototype.sendLog = sendLog

    /**
     * 实例化Seye时合并config
     */
    function init() {
        let _config = global.eye ? global.eye.config ? global.eye.config : {} : {}
        let a = Object.assign(config, _config);
        return a
    }



    if (typeof module !== "undefined" && module.exports) {
        module.exports = new ProxySingletonSeye(init());
    } else if (typeof define === "function" && define.amd) {
        define(function () {
            return new ProxySingletonSeye(init());
        });
    } else {
        !('Seye' in global) && (window.eye = new ProxySingletonSeye(init()));
    }



})(window, document)