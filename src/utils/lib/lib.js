/**
 * 判断是不是https
 */
export function isHttps() {
    return global.location.protocol === 'https:' ? true : false
}

/**
 * 判断是否支持H5Geolocation
 */
export function canUseGeolocation() {
    return global.navigator.geolocation ? global.navigator.geolocation.getCurrentPosition ? true : false : false
}

/**
 * 存入localstorage
 */
export function saveStorage(key, value) {
    let storage = localStorage[key] ? JSON.parse(localStorage[key]) : [];
    storage.push(value);
    localStorage[key] = JSON.stringify(storage);
}

/**
 *
 * 标准时间转换年月日时分秒
 * @param {Date} date  new Date()生成的时间
 * @returns '2018-12-12 12:12:12'
 */
export function translateDate(date) {
    return `${date.getFullYear()}-${(date.getMonth() + 1)>10?date.getMonth()+1:'0'+String(date.getMonth() + 1)}-${date.getDate()>10?date.getDate():'0'+String(date.getDate())} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
}

export function getStringLength(str) {
    var len = 0;
    for (var i = 0; i < str.length; i++) {
        if (str.charCodeAt(i) > 127 || str.charCodeAt(i) == 94) {
            len += 2;
        } else {
            len++;
        }
    }
    return len;
}


// export function setAjaxStorage(ajaxInfo) {
//     console.log(ajaxInfo);
//     let ajaxList = localStorage.ajaxList ? JSON.parse(localStorage.ajaxList) : [];
//     ajaxList.push(ajaxInfo);
//     localStorage.ajaxList = JSON.stringify(ajaxList);

// }



export default {
    saveStorage,
    translateDate,
    canUseGeolocation,
    isHttps,
}