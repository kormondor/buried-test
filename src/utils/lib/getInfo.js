    import {
        isHttps
    } from './lib';

    /**
     *
     *获取浏览器设备型号
     * @returns 浏览器类型
     */
    export const getDevice = function () {
        let DEVICE = ['iPhone', 'iPad', 'Android', 'Windows Phone', 'Chrome', 'MSIE', 'Firefox', 'Opera', 'Safari', 'MicroMessenger'];
        for (let i of DEVICE) {
            if (this.getUA().indexOf(i) > -1) {
                // this.device = i === 'MSIE' ? 'IE' : i === 'MicroMessenger' ? 'Wechat' : i
                // break;
                return i === 'MSIE' ? 'IE' : i === 'MicroMessenger' ? 'Wechat' : i
            }
        }
    }

    /**
     *获取URL
     *
     * @returns
     */
    export const getUrl = function () {
        let urlInfo = {
            href: location.href,
            pathname: location.pathname,
            origin: location.origin,
        }
        // this.urlInfo = urlInfo
        return urlInfo
    }

    /**
     * 获取操作系统
     */
    export const getOS = function () {
        let OS = ['Windows Phone', 'iPhone OS', 'CPU OS', 'Windows', 'Mac OS', 'Android ', 'Linux']
        for (let i of OS) {
            if (this.getUA().indexOf(i) > -1) {
                // this.os = i === 'CPU OS' ? 'IOS' : i
                // break;
                return i === 'CPU OS' ? 'IOS' : i
            }
        }
    }

    /**
     *获取UserAgent
     *
     * @returns
     */
    export const getUA = function () {
        return global.navigator.userAgent
    }

    /**
     * 获取设备的分辨率
     */
    export const getDpi = function () {
        if (global.devicePixelRatio) {
            // this.dpi = `${global.screen.width * global.devicePixelRatio}*${global.screen.height * global.devicePixelRatio}`;
            return `${global.screen.width * global.devicePixelRatio}*${global.screen.height * global.devicePixelRatio}`;
        } else {
            // this.dpi = `${global.screen.width * global}*${global.screen.height}`;
            return `${global.screen.width * global}*${global.screen.height}`;
        }
    }

    /**
     * 获取设备的视口大小
     */
    export const getVp = function () {
        return `${document.documentElement.clientWidth}*${document.documentElement.clientHeight}`
    }

    /**
     *获取用户的定位
     *
     */
    export const getPosition = function () {
        let positon = {};
        let success = function (success) {
            return {
                lat: success.coords.latitude,
                lng: success.coords.longitude
            }
        }.bind(this);
        let fail = function (err) {
            alert(err.message)
        }
        global.navigator.geolocation.getCurrentPosition(success, fail)
    }

    /**
     * 获取性能相关属性
     */
    export const getNetworkPerformance = function () {
        let timing = global.performance.timing;
        if (performance && performance.timing) {
            if (isHttps()) {
                return {
                    fpt: performance.timing.responseEnd - timing.fetchStart,
                    tti: timing.domInteractive - timing.fetchStart,
                    firstbate: timing.responseStart - timing.domainLookupStart,
                    ready: timing.domContentLoadedEventEnd - timing.fetchStart,
                    load: timing.loadEventStart - timing.fetchStart,
                    dns: timing.domainLookupEnd - timing.domainLookupStart,
                    tcp: timing.connectEnd - timing.connectStart,
                    ssl: timing.connectEnd - timing.secureConnectionStart,
                    ttfb: timing.responseStart - timing.requestStart,
                    trans: timing.responseEnd - timing.responseStart,
                    dom: timing.domInteractive - timing.responseEnd,
                    res: timing.loadEventStart - timing.domContentLoadedEventEnd
                }
            } else {
                return {
                    fpt: performance.timing.responseEnd - timing.fetchStart,
                    tti: timing.domInteractive - timing.fetchStart,
                    firstbate: timing.responseStart - timing.domainLookupStart,
                    ready: timing.domContentLoadedEventEnd - timing.fetchStart,
                    load: timing.loadEventStart - timing.fetchStart,
                    dns: timing.domainLookupEnd - timing.domainLookupStart,
                    tcp: timing.connectEnd - timing.connectStart,
                    ttfb: timing.responseStart - timing.requestStart,
                    trans: timing.responseEnd - timing.responseStart,
                    dom: timing.domInteractive - timing.responseEnd,
                    res: timing.loadEventStart - timing.domContentLoadedEventEnd
                }
            }
        } else {
            console.log('请在浏览器端使用本插件');
        }
    }

    export default {
        getPosition,
        getDevice,
        getDpi,
        getUA,
        getUrl,
        getOS,
        getNetworkPerformance,
        getVp
    }