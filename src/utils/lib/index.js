import lib from './lib';
import info from './getInfo';

const utils = {};

export default Object.assign(utils, lib, info)